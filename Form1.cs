﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AB02
{
    public partial class frmPingPong : Form
    {
        private bool godown, goleft;
        private int newx = 100;
        private int newy = 150;
        private int points;
        
        public frmPingPong()
        {
            InitializeComponent();
        }
        private void pnlSpiel_Paint(object sender, PaintEventArgs e)
        {
         
        }

        private void picBall_Click(object sender, EventArgs e)
        {

        }

        private void btnSpielstarten_Click(object sender, EventArgs e)
        {
            Spieltimer.Enabled = true;
        }

        private void Spieltimer_Tick(object sender, EventArgs e)
        {
            lblTime.Text = Convert.ToString(Spieltimer.Interval);
            if (picBall.Location.X + picBall.Width >= pnlSpiel.Location.X)
            {
                for(int i = 0; i<= pnlSpiel.Height; i++)
                {
                    if (picBall.Location.Y + i == pnlSpiel.Location.Y)
                    {
                        points = + 10;
                        textBox1.Text = Convert.ToString(points);
                    }
                }                
            }
            if (picBall.Location.X + picBall.Width > pnlSpiel.Width)
            {
                goleft = true;
            }

            if (picBall.Location.X < pnlSpiel.Location.X)
            {
                goleft = false;
            }

            if (picBall.Location.Y + picBall.Height > pnlSpiel.Height)
            {
                godown = false;
            }

            if (picBall.Location.Y < pnlSpiel.Location.Y)
            {
                godown = true;
            }

            if (Spieltimer.Enabled)
            {
                if (goleft)
                {
                    int newx = picBall.Location.X - 5;
                    picBall.Location = new Point(newx, picBall.Location.Y);
                }

                if (!goleft)
                {
                    int newx = picBall.Location.X + 5;
                    picBall.Location = new Point(newx, picBall.Location.Y);
                }

                if (godown)
                {
                    int newy = picBall.Location.Y + 5;
                    picBall.Location = new Point(picBall.Location.X, newy);
                }

                if (!godown)
                {
                    int newy = picBall.Location.Y - 5;
                    picBall.Location = new Point(picBall.Location.X, newy);
                }
            }
        }

        private void lblTime_Click(object sender, EventArgs e)
        {
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            panel1.Location = new Point(panel1.Location.X,pnlSpiel.Location.Y + pnlSpiel.Height / 100 * vScrollBar1.Value);
        }

        private void frmPingPong_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }
    }
}
